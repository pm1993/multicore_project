/** CARMA with sequential and depth = 1(proc = 2)**/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

#define SPLIT_M 1
#define SPLIT_K 2
#define SPLIT_N 3

#include <pthread.h>

#define A(i,j) a[ (j)*lda + (i) ]
#define B(i,j) b[ (j)*ldb + (i) ]
#define C(i,j) c[ (j)*ldc + (i) ]

struct __attribute__((__packed__)) gemm_params {
    int m;
    int k;
    int n;
    double *A;
    int LDA;
    double *B;
    int LDB;
    double *C;
    int LDC;
    int depth;
    int max_depth;
};
void inner_multiply(int m, int k, int n, double *A, int LDA, double *B, int LDB, double *C, int LDC,int depth,int max_depth);

void *inner_multiply_wrapper(void *p) {
    struct gemm_params *params = p;
    /* Quad code using the 'params' pointer */
    inner_multiply(params->m, params->k, params->n, params->A, params->LDA, params->B, params->LDB, params->C, params->LDC,params->depth,params->max_depth);
    return ;
    }

// Split largest dimension
int dim_to_split(int m, int k, int n) {
    if (n >= k && n >= m) return SPLIT_N;
    if (m >= k && m >= n) return SPLIT_M;
    return SPLIT_K;
}

void normal_gemm( int m, int n, int k, double *a, int lda,
                  double *b, int ldb,
                  double *c, int ldc )
{
    int i, j, p;

    for ( i=0; i<m; i++ ){        /* Loop over the rows of C */
        for ( j=0; j<n; j++ ){        /* Loop over the columns of C */
            for ( p=0; p<k; p++ ){        /* Update C( i,j ) with the inner
				       product of the ith row of A and
				       the jth column of B */
                C( i,j ) = C( i,j ) +  A( i,p ) * B( p,j );
            }
        }
    }
}


void inner_multiply(int m, int k, int n, double *A, int LDA, double *B, int LDB, double *C, int LDC,int depth,int max_depth) {
    if (depth >= max_depth) {
        normal_gemm( m, n, k, A, LDA, B, LDB, C, LDC);
        return;
    }
    pthread_t  th1;
    int next_depth = depth + 1;
    int dim = dim_to_split(m, k, n);
//    struct gemm_params p1;
    if (dim == SPLIT_N) {
        struct gemm_params p1 = {.m = m, .k=k, .n = n/ 2, .A=A, .LDA=LDA, .B=B, .LDB = LDB, .C =C, .LDC=LDC, .depth=next_depth, .max_depth=max_depth};
        if(pthread_create(&th1,NULL,inner_multiply_wrapper,&p1))
        {
            fprintf(stderr, "Error creating thread\n");
            return;
        }

        inner_multiply(m, k, n/2, A, LDA, B + n/2*LDB, LDB, C + n/2*LDC, LDC, next_depth, max_depth);
        if(pthread_join(th1, NULL)) {
            fprintf(stderr, "Error joining thread\n");
            return;
        }

    } else if (dim == SPLIT_M) {
        struct gemm_params p1 = {.m = m/2, .k=k, .n = n, .A=A, .LDA=LDA, .B=B, .LDB = LDB, .C =C, .LDC=LDC, .depth=next_depth, .max_depth=max_depth};
        if(pthread_create(&th1,NULL,&inner_multiply_wrapper,&p1))
        {
            fprintf(stderr, "Error creating thread\n");
            return;
        }

        inner_multiply(m/2, k, n, A + m/2, LDA, B, LDB, C + m/2, LDC, next_depth, max_depth);
        if(pthread_join(th1, NULL)) {
            fprintf(stderr, "Error joining thread\n");
            return;
        }

//        cilk_spawn inner_multiply(m/2, k, n, A, LDA, B, LDB, C, LDC, next_depth, max_depth);
//        inner_multiply(m/2, k, n, A + m/2, LDA, B, LDB, C + m/2, LDC, next_depth, max_depth);
//        cilk_sync;

    } else { // SPLIT_K
        float *C2 = (float*) malloc(m * n * sizeof(float));
        struct gemm_params p1 = {.m = m, .k=k/2, .n = n, .A=A, .LDA=LDA, .B=B, .LDB = LDB, .C =C, .LDC=LDC, .depth=next_depth, .max_depth=max_depth};
        if(pthread_create(&th1,NULL,&inner_multiply_wrapper,&p1))
        {
            fprintf(stderr, "Error creating thread\n");
            return;
        }

        inner_multiply(m, k/2, n, A + k/2*LDA, LDA, B + k/2, LDB, C, LDC, next_depth, max_depth);
        if(pthread_join(th1, NULL)) {
            fprintf(stderr, "Error joining thread\n");
            return;
        }

//        cilk_spawn inner_multiply(m, k/2, n, A, LDA, B, LDB, C2, m, next_depth, max_depth);
//        inner_multiply(m, k/2, n, A + k/2*LDA, LDA, B + k/2, LDB, C, LDC, next_depth, max_depth);
//        cilk_sync;
        int x,z;
        for (x = 0; x < n; x++) {
            for(z = 0; z < m; z++)
            {
                C[LDC*x+z] +=  C2[m*x+z];
            }
//            cblas_saxpy(m, 1, C2 + m*x, 1, C + LDC*x, 1);
        }
        free(C2);
    }
}

void MY_MMult( int m, int n, int k, double *a, int lda,
               double *b, int ldb,
               double *c, int ldc )
{
    inner_multiply( m, k, n, a, lda, b, ldb, c, ldc, 0, 1);
}