#include <stdlib.h>
#include <stdio.h>
/* Create macros so that the matrices are stored in column-major order */

#define A(i,j) a[ (j)*lda + (i) ]
#define B(i,j) b[ (j)*ldb + (i) ]
#define C(i,j) c[ (j)*ldc + (i) ]
#define A_00(i,j) a_00[ (j)*ldc/2 + (i) ]
#define A_01(i,j) a_01[ (j)*ldc/2 + (i) ]
#define A_10(i,j) a_10[ (j)*ldc/2 + (i) ]
#define A_11(i,j) a_11[ (j)*ldc/2 + (i) ]
#define B_00(i,j) b_00[ (j)*ldc/2 + (i) ]
#define B_01(i,j) b_01[ (j)*ldc/2 + (i) ]
#define B_10(i,j) b_10[ (j)*ldc/2 + (i) ]
#define B_11(i,j) b_11[ (j)*ldc/2 + (i) ]
#define C_00(i,j) c_00[ (j)*ldc/2 + (i) ]
#define C_01(i,j) c_01[ (j)*ldc/2 + (i) ]
#define C_10(i,j) c_10[ (j)*ldc/2 + (i) ]
#define C_11(i,j) c_11[ (j)*ldc/2 + (i) ]
#define M1(i,j) m1[ (j)*ldc/2 + (i) ]
#define M2(i,j) m2[ (j)*ldc/2 + (i) ]
#define M3(i,j) m3[ (j)*ldc/2 + (i) ]
#define M4(i,j) m5[ (j)*ldc/2 + (i) ]
#define M5(i,j) m5[ (j)*ldc/2 + (i) ]
#define M6(i,j) m6[ (j)*ldc/2 + (i) ]
#define M7(i,j) m7[ (j)*ldc/2 + (i) ]
#define T1(i,j) t1[ (j)*ldc/2 + (i) ]
#define T2(i,j) t2[ (j)*ldc/2 + (i) ]
#define T3(i,j) t3[ (j)*ldc/2 + (i) ]
#define T4(i,j) t4[ (j)*ldc/2 + (i) ]
#define T5(i,j) t5[ (j)*ldc/2 + (i) ]
#define T6(i,j) t6[ (j)*ldc/2 + (i) ]
#define T7(i,j) t7[ (j)*ldc/2 + (i) ]
#define T8(i,j) t8[ (j)*ldc/2 + (i) ]
#define T9(i,j) t9[ (j)*ldc/2 + (i) ]
#define T10(i,j) t10[ (j)*ldc/2 + (i) ]
#define T11(i,j) t11[ (j)*ldc/2 + (i) ]
#define T12(i,j) t12[ (j)*ldc/2 + (i) ]
#define T13(i,j) t13[ (j)*ldc/2 + (i) ]
#define T14(i,j) t14[ (j)*ldc/2 + (i) ]

void copy_matrix_offset( int ai_off, int aj_off, int bi_off, int ji_off, int m, int n, double *a, int lda, double *b, int ldb )
{
  int i, j;

  omp_set_num_threads(8);
  #pragma omp parallel for
  for ( j=0; j<n; j++ )
    for ( i=0; i<m; i++ )
      B( i+bi_off,j+ji_off ) = A( i+ai_off,j+aj_off );
}

/* Routine for computing C = A * B + C */

void mAdd( int m, int n, int k, double *a, int lda, 
                                    double *b, int ldb,
                                    double *c, int ldc )
{
  int i, j;

  omp_set_num_threads(8);
  #pragma omp parallel for
  for ( i=0; i<m; i++ ){        /* Loop over the rows of C */
    for ( j=0; j<n; j++ ){        /* Loop over the columns of C */
	   C( i,j ) = A( i,j ) + B( i,j );
    }
  }
}

void mSub( int m, int n, int k, double *a, int lda, 
                                    double *b, int ldb,
                                    double *c, int ldc )
{
  int i, j;

  omp_set_num_threads(8);
  #pragma omp parallel for
  for ( i=0; i<m; i++ ){        /* Loop over the rows of C */
    for ( j=0; j<n; j++ ){        /* Loop over the columns of C */
	   C( i,j ) = A( i,j ) - B( i,j );
    }
  }
}

//____________________________________

// Block sizes 
#define mc 256
#define kc 128

#define min( i, j ) ( (i)<(j) ? (i): (j) )

// Routine for computing C = A * B + C 

void AddDot4x4( int, double *, int, double *, int, double *, int );
void PackMatrixA( int, double *, int, double * );

void mMul( int m, int n, int k, double *a, int lda, 
                                    double *b, int ldb,
                                    double *c, int ldc )
{
  int i, p, pb, ib;
  omp_set_num_threads(8);
  // This time, we compute a mc x n block of C by a call to the InnerKernel 

  for ( p=0; p<k; p+=kc ){
    pb = min( k-p, kc );
    for ( i=0; i<m; i+=mc ){
      ib = min( m-i, mc );
      InnerKernel( ib, n, pb, &A( i,p ), lda, &B(p, 0 ), ldb, &C( i,0 ), ldc );
    }
  }
}

void InnerKernel( int m, int n, int k, double *a, int lda, 
                                       double *b, int ldb,
                                       double *c, int ldc )
{
  int i, j;
  double 
    packedA[ m * k ];

  for ( j=0; j<n; j+=4 ){        // Loop over the columns of C, unrolled by 4 
    #pragma omp parallel for
    for ( i=0; i<m; i+=4 ){        // Loop over the rows of C 
      if ( j == 0 ) PackMatrixA( k, &A( i, 0 ), lda, &packedA[ i*k ] );
      AddDot4x4( k, &packedA[ i*k ], 4, &B( 0,j ), ldb, &C( i,j ), ldc );
    }
  }
}

void PackMatrixA( int k, double *a, int lda, double *a_to )
{
  int j;

  for( j=0; j<k; j++){  // loop over columns of A 
    double 
      *a_ij_pntr = &A( 0, j );

    *a_to++ = *a_ij_pntr;
    *a_to++ = *(a_ij_pntr+1);
    *a_to++ = *(a_ij_pntr+2);
    *a_to++ = *(a_ij_pntr+3);
  }
}

#include <mmintrin.h>
#include <xmmintrin.h>  // SSE
#include <pmmintrin.h>  // SSE2
#include <emmintrin.h>  // SSE3

typedef union
{
  __m128d v;
  double d[2];
} v2df_t;

void AddDot4x4( int k, double *a, int lda,  double *b, int ldb, double *c, int ldc )
{

  int p;
  v2df_t
    c_00_c_10_vreg,    c_01_c_11_vreg,    c_02_c_12_vreg,    c_03_c_13_vreg,
    c_20_c_30_vreg,    c_21_c_31_vreg,    c_22_c_32_vreg,    c_23_c_33_vreg,
    a_0p_a_1p_vreg,
    a_2p_a_3p_vreg,
    b_p0_vreg, b_p1_vreg, b_p2_vreg, b_p3_vreg; 

  double 
    // Point to the current elements in the four columns of B 
    *b_p0_pntr, *b_p1_pntr, *b_p2_pntr, *b_p3_pntr; 
    
  b_p0_pntr = &B( 0, 0 );
  b_p1_pntr = &B( 0, 1 );
  b_p2_pntr = &B( 0, 2 );
  b_p3_pntr = &B( 0, 3 );

  c_00_c_10_vreg.v = _mm_setzero_pd();   
  c_01_c_11_vreg.v = _mm_setzero_pd();
  c_02_c_12_vreg.v = _mm_setzero_pd(); 
  c_03_c_13_vreg.v = _mm_setzero_pd(); 
  c_20_c_30_vreg.v = _mm_setzero_pd();   
  c_21_c_31_vreg.v = _mm_setzero_pd();  
  c_22_c_32_vreg.v = _mm_setzero_pd();   
  c_23_c_33_vreg.v = _mm_setzero_pd(); 

  for ( p=0; p<k; p++ ){
    a_0p_a_1p_vreg.v = _mm_load_pd( (double *) a );
    a_2p_a_3p_vreg.v = _mm_load_pd( (double *) ( a+2 ) );
    a += 4;

    b_p0_vreg.v = _mm_loaddup_pd( (double *) b_p0_pntr++ );   // load and duplicate 
    b_p1_vreg.v = _mm_loaddup_pd( (double *) b_p1_pntr++ );   // load and duplicate 
    b_p2_vreg.v = _mm_loaddup_pd( (double *) b_p2_pntr++ );   // load and duplicate 
    b_p3_vreg.v = _mm_loaddup_pd( (double *) b_p3_pntr++ );   // load and duplicate 
    // First row and second rows 
    c_00_c_10_vreg.v += a_0p_a_1p_vreg.v * b_p0_vreg.v;
    c_01_c_11_vreg.v += a_0p_a_1p_vreg.v * b_p1_vreg.v;
    c_02_c_12_vreg.v += a_0p_a_1p_vreg.v * b_p2_vreg.v;
    c_03_c_13_vreg.v += a_0p_a_1p_vreg.v * b_p3_vreg.v;

    // Third and fourth rows 
    c_20_c_30_vreg.v += a_2p_a_3p_vreg.v * b_p0_vreg.v;
    c_21_c_31_vreg.v += a_2p_a_3p_vreg.v * b_p1_vreg.v;
    c_22_c_32_vreg.v += a_2p_a_3p_vreg.v * b_p2_vreg.v;
    c_23_c_33_vreg.v += a_2p_a_3p_vreg.v * b_p3_vreg.v;
  }

  C( 0, 0 ) += c_00_c_10_vreg.d[0];  C( 0, 1 ) += c_01_c_11_vreg.d[0];  
  C( 0, 2 ) += c_02_c_12_vreg.d[0];  C( 0, 3 ) += c_03_c_13_vreg.d[0]; 

  C( 1, 0 ) += c_00_c_10_vreg.d[1];  C( 1, 1 ) += c_01_c_11_vreg.d[1];  
  C( 1, 2 ) += c_02_c_12_vreg.d[1];  C( 1, 3 ) += c_03_c_13_vreg.d[1]; 

  C( 2, 0 ) += c_20_c_30_vreg.d[0];  C( 2, 1 ) += c_21_c_31_vreg.d[0];  
  C( 2, 2 ) += c_22_c_32_vreg.d[0];  C( 2, 3 ) += c_23_c_33_vreg.d[0]; 

  C( 3, 0 ) += c_20_c_30_vreg.d[1];  C( 3, 1 ) += c_21_c_31_vreg.d[1];  
  C( 3, 2 ) += c_22_c_32_vreg.d[1];  C( 3, 3 ) += c_23_c_33_vreg.d[1]; 
}
//____________________________________

/*
void mMul( int m, int n, int k, double *a, int lda, 
                                    double *b, int ldb,
                                    double *c, int ldc )
{
  int i, j, p;

  for ( i=0; i<m; i++ ){
    for ( j=0; j<n; j++ ){
      for ( p=0; p<k; p++ ){
	C( i,j ) = C( i,j ) +  A( i,p ) * B( p,j );
      }
    }
  }
}
*/

void MY_MMult( int m, int n, int k, double *a, int lda, 
                                    double *b, int ldb,
                                    double *c, int ldc )
{
  int i, j, p;
  double *m1, *m2, *m3, *m4, *m5, *m6, *m7;
  double *t1, *t2, *t3, *t4, *t5, *t6, *t7, *t8, *t9, *t10, *t11, *t12, *t13, *t14;
  double *a_00, *a_01, *a_10, *a_11;
  double *b_00, *b_01, *b_10, *b_11;
  double *c_00, *c_01, *c_10, *c_11;

    m1 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    m2 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    m3 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    m4 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    m5 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    m6 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    m7 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    a_00 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    a_01 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    a_10 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    a_11 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    b_00 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    b_01 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    b_10 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    b_11 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    c_00 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    c_01 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    c_10 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    c_11 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t1 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t2 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t3 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t4 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t5 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t6 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t7 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t8 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t9 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t10 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t11 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t12 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t13 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );
    t14 = ( double * ) malloc( ldc/2 * n/2 * sizeof( double ) );

    omp_set_num_threads(8);
    #pragma omp parallel for
    for(i = 0; i < k/2; i++) {
      for(j = 0; j < k/2; j++) {
        M1(i,j) = 0;
        M2(i,j) = 0;
        M3(i,j) = 0;
        M4(i,j) = 0;
        M5(i,j) = 0;
        M6(i,j) = 0;
        M7(i,j) = 0;
        T1(i,j) = 0;
        T2(i,j) = 0;
        T3(i,j) = 0;
        T4(i,j) = 0;
        T5(i,j) = 0;
        T6(i,j) = 0;
        T7(i,j) = 0;
        T8(i,j) = 0;
        T9(i,j) = 0;
        T10(i,j) = 0;
        T11(i,j) = 0;
        T12(i,j) = 0;
        T13(i,j) = 0;
        T14(i,j) = 0;
      }
    }

    copy_matrix_offset(0, 0, 0, 0, m/2, n/2, a, ldc, a_00, ldc/2);
    copy_matrix_offset(0, m/2, 0, 0, m/2, n/2, a, ldc, a_01, ldc/2);
    copy_matrix_offset(m/2, 0, 0, 0, m/2, n/2, a, ldc, a_10, ldc/2);
    copy_matrix_offset(m/2, m/2, 0, 0, m/2, n/2, a, ldc, a_11, ldc/2);

    copy_matrix_offset(0, 0, 0, 0, m/2, n/2, b, ldc, b_00, ldc/2);
    copy_matrix_offset(0, m/2, 0, 0, m/2, n/2, b, ldc, b_01, ldc/2);
    copy_matrix_offset(m/2, 0, 0, 0, m/2, n/2, b, ldc, b_10, ldc/2);
    copy_matrix_offset(m/2, m/2, 0, 0, m/2, n/2, b, ldc, b_11, ldc/2);

    // m1
    mAdd(n/2, n/2, n/2, a_00, ldc/2, a_11, ldc/2, t1, ldc/2);
    mAdd(n/2, n/2, n/2, b_00, ldc/2, b_11, ldc/2, t2, ldc/2);
    mMul(n/2, n/2, n/2, t1, ldc/2, t2, ldc/2, m1, ldc/2);
    // m2
    mAdd(n/2, n/2, n/2, a_10, ldc/2, a_11, ldc/2, t3, ldc/2);
    mMul(n/2, n/2, n/2, t3, ldc/2, b_00, ldc/2, m2, ldc/2);
    // m3
    mSub(n/2, n/2, n/2, b_01, ldc/2, b_11, ldc/2, t4, ldc/2);
    mMul(n/2, n/2, n/2, a_00, ldc/2, t4, ldc/2, m3, ldc/2);
    // m4
    mSub(n/2, n/2, n/2, b_10, ldc/2, b_00, ldc/2, t5, ldc/2);
    mMul(n/2, n/2, n/2, a_11, ldc/2, t5, ldc/2, m4, ldc/2);
    // m5
    mAdd(n/2, n/2, n/2, a_00, ldc/2, a_01, ldc/2, t6, ldc/2);
    mMul(n/2, n/2, n/2, t6, ldc/2, b_11, ldc/2, m5, ldc/2);
    // m6
    mSub(n/2, n/2, n/2, a_10, ldc/2, a_00, ldc/2, t7, ldc/2);
    mAdd(n/2, n/2, n/2, b_00, ldc/2, b_01, ldc/2, t8, ldc/2);
    mMul(n/2, n/2, n/2, t7, ldc/2, t8, ldc/2, m6, ldc/2);
    // m7
    mSub(n/2, n/2, n/2, a_01, ldc/2, a_11, ldc/2, t9, ldc/2);
    mAdd(n/2, n/2, n/2, b_10, ldc/2, b_11, ldc/2, t10, ldc/2);
    mMul(n/2, n/2, n/2, t9, ldc/2, t10, ldc/2, m7, ldc/2);

    // c_00
    mAdd(n/2, n/2, n/2, m1, ldc/2, m4, ldc/2, t11, ldc/2);
    mSub(n/2, n/2, n/2, t11, ldc/2, m5, ldc/2, t12, ldc/2);
    mAdd(n/2, n/2, n/2, t12, ldc/2, m7, ldc/2, c_00, ldc/2);
    copy_matrix_offset(0, 0, 0, 0, m/2, n/2, c_00, ldc/2, c, ldc);

    // c_01
    mAdd(n/2, n/2, n/2, m3, ldc/2, m5, ldc/2, c_01, ldc/2);
    copy_matrix_offset(0, 0, 0, m/2, m/2, n/2, c_01, ldc/2, c, ldc);

    // c_10
    mAdd(n/2, n/2, n/2, m2, ldc/2, m4, ldc/2, c_10, ldc/2);
    copy_matrix_offset(0, 0, m/2, 0, m/2, n/2, c_10, ldc/2, c, ldc);

    // c_11
    mSub(n/2, n/2, n/2, m1, ldc/2, m2, ldc/2, t13, ldc/2);
    mAdd(n/2, n/2, n/2, t13, ldc/2, m3, ldc/2, t14, ldc/2);
    mAdd(n/2, n/2, n/2, t14, ldc/2, m6, ldc/2, c_11, ldc/2);
    copy_matrix_offset(0, 0, m/2, m/2, m/2, n/2, c_11, ldc/2, c, ldc);


  free(m1);
  free(m2);
  free(m3);
  free(m4);
  free(m5);
  free(m6);
  free(m7);
  free(a_00);
  free(a_01);
  free(a_10);
  free(a_11);
  free(b_00);
  free(b_01);
  free(b_10);
  free(b_11);
  free(c_00);
  free(c_01);
  free(c_10);
  free(c_11);
  free(t1);
  free(t2);
  free(t3);
  free(t4);
  free(t5);
  free(t6);
  free(t7);
  free(t8);
  free(t9);
  free(t10);
  free(t11);
  free(t12);
  free(t13);
  free(t14);
}


  
