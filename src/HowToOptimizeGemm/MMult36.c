#include <omp.h>

/*
   omp block-striped implementation with 4 threads
   on top of naive sequential implementation (MMult5)
   basic subtask assigned to a thead is to calculate a row of C
   this is so far the fastest true parallel striped implementation
   also scales well
*/

/* Create macros so that the matrices are stored in column-major order */

#define A(i,j) a[ (j)*lda + (i) ]
#define B(i,j) b[ (j)*ldb + (i) ]
#define C(i,j) c[ (j)*ldc + (i) ]

/* Routine for computing C = A * B + C */

void AddDot( int, double *, int, double *, double * );

void MY_MMult( int m, int n, int k, double *a, int lda, 
                                    double *b, int ldb,
                                    double *c, int ldc )
{
  int i, j, p;
  omp_set_num_threads(4);

  #pragma omp parallel for
  for ( i=0; i<m; i+=1 ){        /* Loop over the rows of C */
    for ( j=0; j<n; j+=4 ){        /* Loop over the columns of C, unrolled by 4 */
      for ( p=0; p<k; p+=4 ){
        C( i, j   ) += A( i, p+0 ) * B( p+0, j );     
        C( i, j+1 ) += A( i, p+0 ) * B( p+0, j+1 );     
        C( i, j+2 ) += A( i, p+0 ) * B( p+0, j+2 );     
        C( i, j+3 ) += A( i, p+0 ) * B( p+0, j+3 );     
        C( i, j   ) += A( i, p+1 ) * B( p+1, j );     
        C( i, j+1 ) += A( i, p+1 ) * B( p+1, j+1 );     
        C( i, j+2 ) += A( i, p+1 ) * B( p+1, j+2 );     
        C( i, j+3 ) += A( i, p+1 ) * B( p+1, j+3 );     
        C( i, j   ) += A( i, p+2 ) * B( p+2, j );     
        C( i, j+1 ) += A( i, p+2 ) * B( p+2, j+1 );     
        C( i, j+2 ) += A( i, p+2 ) * B( p+2, j+2 );     
        C( i, j+3 ) += A( i, p+2 ) * B( p+2, j+3 );     
        C( i, j   ) += A( i, p+3 ) * B( p+3, j );     
        C( i, j+1 ) += A( i, p+3 ) * B( p+3, j+1 );     
        C( i, j+2 ) += A( i, p+3 ) * B( p+3, j+2 );     
        C( i, j+3 ) += A( i, p+3 ) * B( p+3, j+3 );     
      }
    }
  }
}


/* Create macro to let X( i ) equal the ith element of x */

#define X(i) x[ (i)*incx ]

void AddDot( int k, double *x, int incx,  double *y, double *gamma )
{
  /* compute gamma := x' * y + gamma with vectors x and y of length n.

     Here x starts at location x with increment (stride) incx and y starts at location y and has (implicit) stride of 1.
  */
 
  int p;

  for ( p=0; p<k; p++ ){
    *gamma += X( p ) * y[ p ];     
  }
}
